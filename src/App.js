import React from "react";
import Sidebar from "./components/layout/sidebar/Sidebar";
import Header from "./components/layout//header/Header";
import Maindashboard from "./components/content/mainDashboard/Maindashboard";
import Ships from "./components/content/ships/Ships";
import Users from "./components/content/users/Users";

import Grid from "@material-ui/core/Grid";
import { BrowserRouter, Route, Switch } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <div style={{ overflowX: "hidden" }}>
        <Grid container spacing={0} alignContent="flex-start">
          <Grid item xs={12} md={3} lg={2}>
                <Sidebar />
          </Grid>
          <Grid xs={12} md={9} lg={10} container item spacing={0} alignContent="flex-start">
            <Grid item xs={12}>
                <Header />
            </Grid>
            <Grid item xs={12} style={{ padding: 30}}>
                <Switch>
                    <Route exact path='/' component={Maindashboard}/>
                    <Route path='/ships' component={Ships}/>
                    <Route path='/users' component={Users}/>
                </Switch>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </BrowserRouter>
  );
}

export default App;
