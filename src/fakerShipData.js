import faker from 'faker'
import moment from 'moment'
// import { v4 as uuidv4 } from 'uuid'

const fakeDate = ({ date = undefined, min, max }) => moment(date).add(faker.random.number({ min, max }), 'day')

const fakeCargoItems = () => [...Array(faker.random.number({ min: 10, max: 20 })).keys()]
  .map(_ => ({
    // uuid: uuidv4(),
    name: faker.lorem.word(),
    weight: faker.random.number({ min: 10, max: 50 })
  }))



const SHIP_TYPES = [
  'cargo',
  'cruise_ship',
  'yacht',
  'slave_ship',
  'battleship',
  'carrier',
  'trireme',
  'ironclad',
  'frigate',
  'boat'
]

const CAN_SHIP_CARGO = [
  'cargo',
  'cruise_ship',
  'boat',
  'slave_ship'
]

const shipsData = [
  ...Array(
    faker.random.number({ min: 5, max: 25 })
  ).keys()
].map(_ => {
  const arrivalDate = fakeDate({ min: -31, max: 31 }).format('YYYY-MM-DD')
  const departureDate = fakeDate({ date: arrivalDate, min: 31, max: 60 }).format('YYYY-MM-DD')

  const type = faker.random.arrayElement(SHIP_TYPES)
  const canShipCargo = CAN_SHIP_CARGO.indexOf(type) !== -1

  return {
    // uuid: uuidv4(),
    name: faker.name.firstName(),
    
    country: faker.address.country(),
    // Items currently in the ship
    cargo: canShipCargo ? fakeCargoItems() : [],

    cargoCapacity: faker.random.number({ min: 3000, max: 15000 }),

    // Ship's arrival & departure dates
    dates: {
      arrival: arrivalDate,
      departure: departureDate
    },
    loadingStatus: faker.random.number({ min: 0, max: 100 }),
    type
  }
})

export default shipsData