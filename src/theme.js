import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#4980FF'
    },
    secondary: {
      main: '#FF8743'
    }
  },
  text: {
    primary: '#202224'
  },
  typography: {
    fontFamily: [
      'Nunito Sans',
      'Arial',
      'sans-serif'
    ].join(','),
  },
  spacing: 10,

  
});

export default theme;