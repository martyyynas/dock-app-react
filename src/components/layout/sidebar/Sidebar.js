import React from "react";
import { NavLink, Link } from 'react-router-dom';
import { List, ListItem, Typography } from "@material-ui/core";
import DashboardIcon from '@material-ui/icons/Dashboard';
import DirectionsBoatIcon from '@material-ui/icons/DirectionsBoat';
import PeopleIcon from '@material-ui/icons/People';
import logo from '../../../images/wheel.png';
import './sidebar.scss';


export default function Sidebar() {
  return (
    <aside className="sidebar">
      <Link to="/" style={{ textDecoration: 'none', color: '#202224'}}>
        <div className="logo" >
          <img src={logo} alt="Logo" />
          <Typography component="span">DOCK</Typography>
        </div>
      </Link>
        <List>
            <ListItem className="list-item">
                <NavLink exact activeClassName='is-active' to='/' className="nav-item">
                    <DashboardIcon className="nav-ico"/>
                    <Typography component="p">Dashboard</Typography>
                    
                </NavLink>
            </ListItem>
            <ListItem className="list-item">
                <NavLink activeClassName='is-active' to='/ships' className="nav-item">
                    <DirectionsBoatIcon className="nav-ico"/>
                    <Typography component="p">Ships</Typography>
                </NavLink>
            </ListItem>
            <ListItem className="list-item">
                <NavLink activeClassName='is-active' to='/users' className="nav-item">
                    <PeopleIcon className="nav-ico"/>
                    <Typography component="p">Users</Typography>
                </NavLink>
            </ListItem>
        </List>
    </aside>
  );
}

