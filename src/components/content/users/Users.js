import React, { useState, useEffect } from 'react'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import axios from 'axios'
import './users.scss'

const Users = () => {
  const [users, setUsers] = useState([])

  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/users')
      .then(res => {
        setUsers(res.data)
      })
      .catch(err => {
        console.log(err)
      })
  }, [])


  const user = users.map(user => 
    (
      <Grid item className="user" key={user.id} xs={3}>
          <Paper className="paper">
            <h2>{user.name}</h2>
            <a href="#">{user.email}</a>
            <p>{user.phone}</p>
          </Paper>
      </Grid>
  ))

  return (
    <Grid container spacing={3}>
        {user}
    </Grid>
  )
}

export default Users
